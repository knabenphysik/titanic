# Machine Learning 101

Kaggle [Titanic Machine Learning from Disaster](https://www.kaggle.com/competitions/titanic/overview) is considered as the first step into the realm of Data Science.

This repo is to highlight few important steps before performing the _actual_ machine learning task.

> Note: most of codes are taken from [here](https://www.kaggle.com/competitions/titanic/code)


## Start : Exploratory Data Analysis

- [ ] How to view the data and **_find_** its features relationship 
- [ ] Is the data `clean` and `balance` ?


## Machine Learning Modelling

- [ ] Algo 1
- [ ] Algo 2

## Model Evaluation

- [ ] Measurement


## Nice Reporting

Refer [here](https://quarto.org/docs/get-started/hello/jupyter.html) for nice reporting as PDF or HTML